-- General text and comments colors.
vim.api.nvim_set_hl(0, 'Normal', { ctermfg = 247 })
vim.api.nvim_set_hl(0, 'Comment', { ctermfg = 239 })

-- Number literal colors.
vim.api.nvim_set_hl(0, 'Number', { ctermfg = 9 })

-- Other literal colors.
vim.api.nvim_set_hl(0, 'Boolean', { ctermfg = 9, italic = true })
vim.api.nvim_set_hl(0, 'String', { ctermfg = 1 })

-- Namespaces and constants.
vim.api.nvim_set_hl(0, 'Constant', { ctermfg = 98, italic = true })

-- Function names and statements.
vim.api.nvim_set_hl(0, 'Function', { ctermfg = 28 })
vim.api.nvim_set_hl(0, 'Statement', { ctermfg = 136 })

-- Branch and loop colors.
vim.api.nvim_set_hl(0, 'Conditional', { ctermfg = 164, bold = true })
vim.api.nvim_set_hl(0, 'Repeat', { ctermfg = 164, bold = true })

-- Preprocessor command colors.
vim.api.nvim_set_hl(0, 'Preproc', { ctermfg = 162 })

-- Type and some keyword colors.
vim.api.nvim_set_hl(0, 'Type', { ctermfg = 26 })

-- Special characters (i.e. escaped characters) colors.
vim.api.nvim_set_hl(0, 'Special', { ctermfg = 208, bold = true })

-- Parenthesis matching colors.
vim.api.nvim_set_hl(0, 'MatchParen', { ctermfg = 202, bold = true })

-- Error and warning colors.
vim.api.nvim_set_hl(0, 'Error', { ctermfg = 160, bold = true })
vim.api.nvim_set_hl(0, 'Todo', { ctermfg = 226, bold = true })

-- Messages colors.
vim.api.nvim_set_hl(0, 'ErrorMsg', { ctermfg = 160 })
vim.api.nvim_set_hl(0, 'WarningMsg', { ctermfg = 184 })
vim.api.nvim_set_hl(0, 'MoreMsg', { ctermfg = 15 })

-- Status line colors.
vim.api.nvim_set_hl(0, 'StatusLineNC', { ctermfg = 239, ctermbg = 233 })
vim.api.nvim_set_hl(0, 'StatusLine', { ctermfg = 247, ctermbg = 233 })

-- Vertical split colors.
vim.api.nvim_set_hl(0, 'VertSplit', { ctermfg = 255 })

-- Wild menu colors (command completion menu).
vim.api.nvim_set_hl(0, 'WildMenu', { ctermfg = 247 })

-- Popup menu colors.
vim.api.nvim_set_hl(0, 'Pmenu', { ctermfg = 251, ctermbg = 235 })
vim.api.nvim_set_hl(0, 'PmenuExtra', { ctermfg = 251, ctermbg = 235 })
vim.api.nvim_set_hl(0, 'PmenuSel', { ctermfg = 172, ctermbg = 234, bold = true })
vim.api.nvim_set_hl(0, 'PmenuSbar', { ctermfg = 251 })
vim.api.nvim_set_hl(0, 'PmenuThumb', { ctermfg = 251, ctermbg = 234 })

-- Tab line colors.
vim.api.nvim_set_hl(0, 'TabLine', { ctermfg = 246, ctermbg = 234, bold = true })
vim.api.nvim_set_hl(0, 'TabLineFill', { ctermbg = 234 })
vim.api.nvim_set_hl(0, 'TabLineSel', { ctermfg = 172, bold = true })

-- Sign column color.
vim.api.nvim_set_hl(0, 'SignColumn', { ctermfg = 247 })

-- Line number colors.
vim.api.nvim_set_hl(0, 'CursorLineNr', { ctermfg = 238, bold = true })
vim.api.nvim_set_hl(0, 'LineNr', { ctermfg = 238 })

-- Spelling error color.
vim.api.nvim_set_hl(0, 'SpellBad', { ctermfg = 255, ctermbg = 52 })
vim.api.nvim_set_hl(0, 'SpellCap', { ctermfg = 255, ctermbg = 58 })

-- Visual mode color.
vim.api.nvim_set_hl(0, 'Visual', { ctermbg = 238 })

-- Identifier.
vim.api.nvim_set_hl(0, 'Identifier', { ctermfg = 247 })

-- Error and warning highlighting.
vim.api.nvim_set_hl(0, 'DiagnosticError', { ctermfg = 160 })
vim.api.nvim_set_hl(0, 'DiagnosticUnderlineError', { ctermfg = 0, ctermbg = 160 })
vim.api.nvim_set_hl(0, 'DiagnosticWarn', { ctermfg = 11 })
vim.api.nvim_set_hl(0, 'DiagnosticUnderlineWarn', { ctermfg = 0, ctermbg = 11 })
vim.api.nvim_set_hl(0, 'DiagnosticUnnecessary', { link = 'DiagnosticUnderlineWarn' })

-- Treesitter nodes for C++.
vim.api.nvim_set_hl(0, '@module', { link = 'Constant' })
vim.api.nvim_set_hl(0, '@keyword.import.cpp', { link = 'Preproc' })
vim.api.nvim_set_hl(0, '@keyword.directive.cpp', { link = 'Preproc' })
vim.api.nvim_set_hl(0, '@keyword.directive.define.cpp', { link = 'Preproc' })
vim.api.nvim_set_hl(0, '@lsp.type.namespace', { link = 'Constant' })
vim.api.nvim_set_hl(0, '@lsp.type.comment.cpp', { link = 'Constant' })
vim.api.nvim_set_hl(0, '@constructor', { link = 'Function' })
vim.api.nvim_set_hl(0, '@attribute', { link = 'Preproc' })
vim.api.nvim_set_hl(0, '@variable.builtin.cpp', { link = 'Special' })
vim.api.nvim_set_hl(0, '@type.qualifier.cpp', { link = 'Keyword' })
vim.api.nvim_set_hl(0, '@storageclass.cpp', { link = 'Keyword' })
vim.api.nvim_set_hl(0, '@keyword.repeat.cpp', { link = 'Repeat' })
vim.api.nvim_set_hl(0, '@keyword.conditional.cpp', { link = 'Conditional' })

-- Treesitter nodes for Ledger.
vim.api.nvim_set_hl(0, '@text.literal.ledger', { link = 'Type' })
vim.api.nvim_set_hl(0, '@field.ledger', { link = 'Function' })

-- Treesitter nodes for Markdown.
vim.api.nvim_set_hl(0, '@markup.raw.block.markdown', { link = 'Function' })
vim.api.nvim_set_hl(0, '@markup.raw.markdown_inline', { link = 'Function' })
vim.api.nvim_set_hl(0, '@markup.heading.1.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.2.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.3.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.4.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.5.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.6.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.1.marker.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.2.marker.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.3.marker.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.4.marker.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.5.marker.markdown', { link = 'Type' })
vim.api.nvim_set_hl(0, '@markup.heading.6.marker.markdown', { link = 'Type' })

-- Treesitter nodes for LaTeX.
vim.api.nvim_set_hl(0, '@text.environment.latex', { link = 'Type' })
vim.api.nvim_set_hl(0, '@text.environment.name.latex', { bold = true })
