#!/bin/sh
# Displays text in the overlay. Displays current image index, total number of
# images, path of the open image relative to the home directory and image
# dimensions.
echo \[${imv_current_index}/${imv_file_count}\] \
     \~/$(realpath --relative-to="$HOME" "${imv_current_file}") \
     \[${imv_width}x${imv_height}\]
