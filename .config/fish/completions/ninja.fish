source /usr/share/fish/completions/ninja.fish

# Override built-in function for fetching ninja targets so that object and
# library files are also provided by the ninja completion.
function __fish_print_ninja_targets
    __fish_ninja -t targets all 2>/dev/null |
        string replace -r ':.*' ''          |
        string match -v -r '(?:^/|cmake)'
end
