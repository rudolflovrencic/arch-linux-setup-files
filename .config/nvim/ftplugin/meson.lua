-- Use 4 spaces for tabs.
vim.bo.shiftwidth = 4
vim.bo.tabstop    = 4
