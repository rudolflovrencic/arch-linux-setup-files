# Make cal output start with Monday.
alias cal='cal -m'

# Make calcurse use custom configuration directory and store files to the cloud.
alias calcurse="calcurse -C $XDG_CONFIG_HOME/calcurse \
                         -D $XDG_CLOUD_HOME/.calcurse"

# Make gdb not print introductory and copyright messages.
alias gdb='gdb -quiet'

# Make mbsync use mail directory defined by XDG user directories and
# configuration file from the PC specific directory.
alias mbsync="mbsync --config $XDG_CONFIG_HOME/pc-specifics/mail/mbsyncrc"

# Make msmtp use configuration file from the PC specific directory.
alias msmtp="msmtp -C $XDG_CONFIG_HOME/pc-specifics/mail/msmtprc"

# Make neomutt unlock the keyring and start the mailbox timer service.
alias neomutt="$XDG_CONFIG_HOME/neomutt/launch.sh"

# Git command for accessing dotfiles git repository.
alias gitdotfiles="git --git-dir=$HOME/.gitdotfiles/ --work-tree=$HOME"

# Application launcher.
alias bemenu-run="$XDG_CONFIG_HOME/bemenu/bemenu-run.sh"

# Command to run synchronization of the cloud files.
alias ncsync="$XDG_CONFIG_HOME/nextcloud/cloudsync.sh"
