# --------------------------------- BASICS -------------------------------------
# Default text editor.
set -gx EDITOR 'nvim'
set -gx VISUAL 'nvim'

# Default web browser.
set -gx BROWSER 'firefox'

# Default terminal emulator.
set -gx TERMINAL_EMULATOR 'alacritty'

# Make Java GUI programs work properly with tiling window managers.
set -gx _JAVA_AWT_WM_NONREPARENTING 1

# Dynamic link libraries link path (shared libraries).
set -gx LD_LIBRARY_PATH '/usr/local/lib'

# Set locale.
set -gx LC_CTYPE "en_US.UTF-8"

# Set GPG TTY.
set -gx GPG_TTY $(tty)
# ------------------------------------------------------------------------------


# ---------------------------- IMPORTANT DIRECTORIES ---------------------------
# Configuration files directory.
set -gx XDG_CONFIG_HOME "$HOME/.config"

# Data files directory.
set -gx XDG_DATA_HOME "$HOME/.local/share"

# Cache directory.
set -gx XDG_CACHE_HOME "$HOME/.cache"

# Mail directory (not defined by XDG specification).
set -gx XDG_MAIL_HOME "$HOME/.local/share/mail"

# Cloud directory (not defined by XDG specification).
set -gx XDG_CLOUD_HOME "$HOME/cloud"
# ------------------------------------------------------------------------------


# ------------------------------ NNN CONFIGURATION -----------------------------
# Put files to trash instead of deleting them directly.
set -gx NNN_TRASH 1

# Bookmarks.
set -gx NNN_BMS (string join ';' \
    "h:$HOME"                    \
    "s:$HOME/docs/fer/phd"       \
    "d:$HOME/docs/"              \
    "D:$HOME/dl/"                \
    "m:$HOME/music/"             \
    "P:$HOME/pics/"              \
    "v:$HOME/vids/"              \
    "p:$HOME/dev/"               \
    "w:$HOME/docs/work/lumo_lab" \
    "u:/run/media/$USER"         \
    "c:$HOME/cloud/")
# ------------------------------------------------------------------------------


# --------------------------- NEXTCLOUD CONFIGURATION --------------------------
set -gx NEXTCLOUD_PASSWORD_STORE 'nextcloud/webo'
set -gx NEXTCLOUD_USERNAME       'rudolf@lovrencic.xyz'
set -gx NEXTCLOUD_SERVER_ADDRESS 'https://nextcloud.webo.hosting'
# ------------------------------------------------------------------------------


# ------------------------------ GO CONFIGURATION ------------------------------
set -gx GOPATH "$XDG_DATA_HOME/go"
# ------------------------------------------------------------------------------


# ------------------------ GCC AND CMAKE CONFIGURATION -------------------------
# Colors of GCC output.
set -gx GCC_COLORS (string join ':' \
    'error=01;31' 'warning=00;33' 'note=03;37' 'locus=01;37' 'quote=00;36')

# Set reasonable CMake defaults.
set -gx CMAKE_C_COMPILER_LAUNCHER     'ccache'
set -gx CMAKE_CXX_COMPILER_LAUNCHER   'ccache'
set -gx CMAKE_GENERATOR               'Ninja'
set -gx CMAKE_BUILD_TYPE              'Debug'
set -gx CMAKE_EXPORT_COMPILE_COMMANDS 'true'
set -gx CMAKE_COLOR_DIAGNOSTICS       'true'
# ------------------------------------------------------------------------------


# ------------------------------- TOOLKIT BACKENDS -----------------------------
# Enable firefox wayland backend.
set -gx MOZ_ENABLE_WAYLAND 1

# Unset the wayland backend.
set --erase QT_QPA_PLATFORM

# Notify pipewire applications that sway is used.
set -gx XDG_CURRENT_DESKTOP 'sway'
# ------------------------------------------------------------------------------


# --------------------------------- IDF SETTINGS -------------------------------
set -gx IDF_TARGET     'esp32'
set -gx IDF_PATH       "$HOME/dev/esp/esp-idf"
set -gx ADF_PATH       "$HOME/dev/esp/esp-adf"
set -gx IDF_TOOLS_PATH "$HOME/.espressif"
# ------------------------------------------------------------------------------
