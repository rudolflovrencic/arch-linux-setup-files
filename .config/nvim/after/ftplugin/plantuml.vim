" Change highligting of colon line since it is linked to a comment by default.
highlight default link plantumlColonLine Function

" Change highligting of divider since it is linked to a comment by default.
highlight default link plantumlSequenceDivider Special
