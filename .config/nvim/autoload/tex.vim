" Open header matching class or tex file in a desired split.
function! tex#OpenMatchingClassOrTex(split_type)
    let l:replacements = { 'cls' : 'tex', 'tex' : 'cls'}

    let l:extension = expand('%:e')
    if !has_key(l:replacements, l:extension)
        echo 'File extension not recognized.'
        return
    endif

    let l:file_to_open  = substitute( @%,
                              \ '.' . l:extension . '$',
                              \ '.' . l:replacements[l:extension],
                              \ '')
    if l:file_to_open == @%
        echo 'Failed to replace file extension.'
        return
    endif

    if !filereadable(l:file_to_open)
        echo 'The following file could not be opened:' l:file_to_open
        return
    endif

   execute a:split_type " | edit " l:file_to_open
endfun
