# ----------------------------------- ZATHURA ----------------------------------
xdg-mime default zathura.desktop \
    application/pdf application/oxps application/epub+zip
# ------------------------------------------------------------------------------


# ------------------------------------- IMV ------------------------------------
xdg-mime default imv-dir.desktop image/jpeg \
    image/bmp image/gif image/jpeg image/jpg image/pjpeg image/png image/tiff \
    image/x-bmp image/x-pcx image/x-png image/x-portable-anymap               \
    image/x-portable-bitmap image/x-portable-graymap image/x-portable-pixmap  \
    image/x-tga image/x-xbitmap
# ------------------------------------------------------------------------------


# ---------------------------------- INKSCAPE ----------------------------------
xdg-mime default inkscape.desktop \
    image/svg+xml image/svg+xml-compressed
# ------------------------------------------------------------------------------


# ------------------------------------ GIMP ------------------------------------
xdg-mime default gimp.desktop \
    image/x-xcf application/x-gzip application/x-xz application/x-bzip2
# ------------------------------------------------------------------------------


# ------------------------------------ MPV -------------------------------------
xdg-mime default mpv-terminal.desktop \
    audio/mpeg audio/x-wav audio/flac application/x-font-gdos \
    application/octet-stream
# ------------------------------------------------------------------------------
